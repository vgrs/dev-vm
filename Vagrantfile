# -*- mode: ruby -*-
# vi: set ft=ruby :

# define vm IP address
## IP = "10.0.0.0"
SYNC_FOLDER = "D:\\"
SYNC_FOLDER_C = "C:\\"
HOSTNAME = "dev.vm"

$configure_user_groups = <<-SCRIPT
	groupadd developers
	useradd -p $(openssl passwd -1 dev) dev -G wheel,developers
SCRIPT


$remove_utility_tools = <<-SCRIPT
	sudo yum -y remove net-tools
	sudo yum -y remove telnet telnet-server
  	sudo yum -y remove tmux
	sudo yum -y remove zip
	sudo yum -y remove unzip
	sudo yum -y remove vim
	sudo yum -y remove wget
SCRIPT

$install_utility_tools = <<-SCRIPT
	sudo yum -y install net-tools
	sudo yum -y install telnet telnet-server
  	sudo yum -y install tmux
	sudo yum -y install zip  unzip
	sudo yum -y install vim
	sudo yum -y install wget
SCRIPT


$remove_java = <<-SCRIPT
	sudo yum -y remove java-1.8.0-openjdk-devel
SCRIPT

$install_java = <<-SCRIPT
	sudo yum -y install java-1.8.0-openjdk-devel
	
	sed -i '/<--adding-java_home-to-path$/d' /home/dev/.bash_profile

	sudo -u dev echo 'export JAVA_HOME=/usr/lib/jvm/java-1.8.0-openjdk-1.8.0.242.b08-0.el7_7.x86_64 #<--adding-java_home-to-path' >> /home/dev/.bash_profile

	sudo -u dev echo 'export PATH=$JAVA_HOME/bin:$PATH #<--adding-java_home-to-path' >> /home/dev/.bash_profile

SCRIPT

$remove_tomcat = <<-SCRIPT
	rm -rf /opt/appservers/apache-tomcat-8.5.54
SCRIPT

$install_tomcat = <<-SCRIPT
	cd /tmp
	wget https://apache.mirrors.tworzy.net/tomcat/tomcat-8/v8.5.54/bin/apache-tomcat-8.5.54.zip
	mkdir /opt/appservers
	unzip apache-tomcat-8.5.54.zip -d /opt/appservers
	rm -rf apache-tomcat-8.5.54.zip
	cd /opt/appservers
	chown -R dev:developers apache-tomcat-8.5.54
	cd apache-tomcat-8.5.54/bin
	sudo -u dev touch setenv.sh
	sudo -u dev echo 'export JPDA_OPTS=-agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=9196' >> setenv.sh

	chmod +x *.sh
SCRIPT

$start_tomcat = <<-SCRIPT
	sudo -u dev /opt/appservers/apache-tomcat-8.5.54/bin/startup.sh
SCRIPT

$stop_tomcat = <<-SCRIPT
	sudo -u dev /opt/appservers/apache-tomcat-8.5.54/bin/shutdown.sh
SCRIPT

$remove_maven = <<-SCRIPT
	rm -rf /opt/buildtools/maven
SCRIPT

$install_maven = <<-SCRIPT
	cd /tmp
	wget http://ftp.ps.pl/pub/apache/maven/maven-3/3.6.3/binaries/apache-maven-3.6.3-bin.zip
	
	mkdir -p /u /opt/buildtools/maven
	unzip apache-maven-3.6.3-bin.zip -d /opt/buildtools/maven
	rm -rf apache-maven-3.6.3-bin.zip
	cd /opt/buildtools/maven
	chown -R dev:developers apache-maven-3.6.3
	cd apache-maven-3.6.3/bin
	chmod +x mvn

	sed -i '/--adding-maven-to-path$/d' /home/dev/.bash_profile

	sudo -u dev echo 'export M2_HOME=/opt/buildtools/maven/apache-maven-3.6.3 # --adding-maven-to-path' >> /home/dev/.bash_profile
	sudo -u dev echo 'export M2=$M2_HOME/bin # --adding-maven-to-path' >> /home/dev/.bash_profile
	sudo -u dev echo 'export PATH=$M2:$PATH # --adding-maven-to-path' >> /home/dev/.bash_profile

SCRIPT

$remove_solr = <<-SCRIPT
	rm -rf /usr/local/solr
SCRIPT

$install_solr = <<-SCRIPT
	cd /tmp
	wget http://ftp.byfly.by/pub/apache.org/lucene/solr/8.5.0/solr-8.5.0.zip
	unzip solr-8.5.0.zip -d /opt/solr
	rm -rf solr-8.5.0.zip 
	cd /opt/solr
	chown -R dev:developers solr-8.5.0/
	cd solr-8.5.0/bin
	chmod +x solr
SCRIPT

$start_solr = <<-SCRIPT
sudo -u dev /usr/local/solr/bin/solr start
SCRIPT

$stop_solr = <<-SCRIPT
	sudo -u dev /usr/local/solr/bin/solr stop -all
SCRIPT

$remove_docker = <<-SCRIPT
 	sudo yum remove docker \
	docker-client \
    	docker-client-latest \
    	docker-common \
    	docker-latest \
    	docker-latest-logrotate \
    	docker-logrotate \
    	docker-selinux \
    	docker-engine-selinux \
    	docker-engine
SCRIPT

$install_docker = <<-SCRIPT
	sudo yum install -y yum-utils device-mapper-persistent-data lvm2

	sudo yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
	
	sudo yum -y install docker-ce
SCRIPT

$start_docker = <<-SCRIPT
	sudo systemctl start docker
SCRIPT

$create_postgres = <<-SCRIPT
	# Delete the containers
	docker rm -f postgres

    	# Install postgres docker image and create container
	docker run --name=postgres -e POSTGRES_PASSWORD=1q2w3e4r5t -p 5432:5432 -d postgres:10.7

	# Install the postres client to host dev.vm
	yum -y install https://download.postgresql.org/pub/repos/yum/reporpms/EL-7-x86_64/pgdg-redhat-repo-latest.noarch.rpm
	
	yum -y install postgresql10
SCRIPT

$start_postgres = <<-SCRIPT
	# Start postgres docker container
	docker container start postgres 
SCRIPT

$stop_postgres = <<-SCRIPT
	# Stop postgres docker container
    	docker container stop postgres 
SCRIPT

$create_mysql = <<-SCRIPT
	# Delete the containers
	docker rm -f mysql

    	# Install postgres docker image and create container
	docker run --name=mysql -e MYSQL_ROOT_PASSWORD=1q2w3e4r5t -p 3306:3306 -d mysql

	# Install mysql client
	yum -y install mysql
	SCRIPT

$start_mysql = <<-SCRIPT
	# Start mysql docker container
    	docker container start mysql 
SCRIPT

$stop_mysql = <<-SCRIPT
	# Install postgres docker image and create container
    	docker container stop mysql 
SCRIPT

$create_mongo = <<-SCRIPT
	# Delete mongo container (if exists)
	docker rm -f mongo

	# Install mongoDB docker image and create container
	docker run --name=mongo -e MONGO_INITDB_ROOT_USERNAME=mongo_user -e MONGO_INITDB_ROOT_PASSWORD=1q2w3e4r5t -e MONGO_INITDB_DATABASE=dev -p 27017:27017 -d mongo\


	# Create mongo client
	rm -rf /usr/local/mongodb/
	mkdir -p /u /usr/local/mongodb/bin
	cd /usr/local/mongodb/bin

	echo "# mongo" > mongo.sh
	echo "docker exec -it mongo bash -c 'mongo'" >> mongo.sh
	
	chmod +x mongo.sh

	sed -i '/^alias mongo=/ d' /home/dev/.bash_profile

	sudo -u dev echo "" >> /home/dev/.bash_profile
	sudo -u dev echo "alias mongo='sudo /usr/local/mongodb/bin/mongo.sh'" >> /home/dev/.bash_profile

SCRIPT

$start_mongo = <<-SCRIPT
	docker container start mongo
SCRIPT

$stop_mongo = <<-SCRIPT
	docker container stop mongo
SCRIPT

$create_rabbitmq = <<-SCRIPT
    	# Delete the containers
    	docker rm -f rabbitmq
	
    	# Install rabbitmq docker image and create container
	docker run -d --hostname dev-rabbitmq --name rabbitmq -p 15672:15672 rabbitmq:3-management
SCRIPT

$start_rabbitmq = <<-SCRIPT
    	docker container start rabbitmq
SCRIPT

$stop_rabbitmq = <<-SCRIPT
    	docker container stop rabbitmq
SCRIPT

$create_couchbase = <<-SCRIPT
    	# Delete the containers
    	docker rm -f couchbase

    	# Install couchbase docker image and create container
    	docker run -d --name couchbase -p 8091-8094:8091-8094 -p 11210:11210 couchbase:enterprise-6.0.2
SCRIPT

$start_couchbase = <<-SCRIPT
    	docker container start couchbase
SCRIPT

$stop_couchbase = <<-SCRIPT
    	docker container stop couchbase
SCRIPT

$create_nginx = <<-SCRIPT
	
	sudo yum -y install epel-release
	sudo yum -y install nginx

SCRIPT

$start_nginx = <<-SCRIPT
	sudo systemctl start nginx
SCRIPT

$stop_nginx = <<-SCRIPT
	sudo systemctl start nginx
SCRIPT

# All Vagrant configuration is done below. The "2" in Vagrant.configure
# configures the configuration version (we support older styles for
# backwards compatibility). Please don't change it unless you know what
# you're doing.
Vagrant.configure("2") do |config|
  # The most common configuration options are documented and commented below.
  # For a complete reference, please see the online documentation at
  # https://docs.vagrantup.com.

  # Every Vagrant development environment requires a box. You can search for
  # boxes at https://vagrantcloud.com/search.
  config.vm.box = "centos/7"

  # Disable automatic box update checking. If you disable this, then
  # boxes will only be checked for updates when the user runs
  # `vagrant box outdated`. This is not recommended.
  # config.vm.box_check_update = false

  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine. In the example below,
  # accessing "localhost:8080" will access port 80 on the guest machine.
  # NOTE: This will enable public access to the opened port
  # config.vm.network "forwarded_port", guest: 80, host: 8080 
  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine and only allow access
  # via 127.0.0.1 to disable public access
  # config.vm.network "forwarded_port", guest: 80, host: 8080, host_ip: "127.0.0.1"

  # Create a private network, which allows host-only access to the machine 
  
  ##config.vm.network "private_network", type: "dhcp"
  ##config.vm.network "private_network", ip: "192.168.50.4"

  # Configure hostname of vm
  config.vm.hostname = HOSTNAME

  # Create a public network, which generally matched to bridged network.
  # Bridged networks make the machine appear as another physical device on
  # your network.
    config.vm.network "public_network"

  # Share an additional folder to the guest VM. The first argument is
  # the path on the host to the actual folder. The second argument is
  # the path on the guest to mount the folder. And the optional third
  # argument is a set of non-required options.
  config.vm.synced_folder SYNC_FOLDER, "/mnt/d"
  config.vm.synced_folder SYNC_FOLDER_C, "/mnt/c"
  # Provider-specific configuration so you can fine-tune various
  # backing providers for Vagrant. These expose provider-specific options.
  # Example for VirtualBox:
  #
  config.vm.provider "virtualbox" do |vb|
  #   # Display the VirtualBox GUI when booting the machine
  #   vb.gui = true
  #
  #   # Customize the amount of memory on the VM:
     vb.memory = "1024"
  #
  #   # Customize the name of the VM:
     vb.name = "dev.vm"
  end
  #
  # View the documentation for the provider you are using for more
  # information on available options.

  # Enable provisioning with a shell script. Additional provisioners such as
  # Puppet, Chef, Ansible, Salt, and Docker are also available. Please see the
  # documentation for more information about their specific syntax and use.
	config.vm.provision "configure-user-groups", type:"shell", inline: $configure_user_groups

	config.vm.provision "remove-utility-tools", type:"shell", inline: $remove_utility_tools

	config.vm.provision "install-utility-tools", type: "shell", inline: $install_utility_tools

	config.vm.provision "remove-java", type: "shell", inline: $remove_java

	config.vm.provision "install-java", type: "shell", inline: $install_java

	config.vm.provision "remove-tomcat", type:"shell", inline: $remove_tomcat

	config.vm.provision "install-tomcat", type:"shell", inline: $install_tomcat

	config.vm.provision "remove-maven", type:"shell", inline: $remove_maven

	config.vm.provision "install-maven", type:"shell", inline: $install_maven

	config.vm.provision "start-tomcat", type:"shell", run: "never", inline: $start_tomcat

	config.vm.provision "stop-tomcat", type:"shell", run: "never", inline: $stop_tomcat

	config.vm.provision "remove-solr", type:"shell", inline: $remove_solr

	config.vm.provision "install-solr", type:"shell", inline: $install_solr

	config.vm.provision "start-solr", type:"shell", run: "never", inline: $start_solr

	config.vm.provision "stop-solr", type:"shell", run: "never", inline: $stop_solr

  	config.vm.provision "remove-docker", type: "shell", inline: $remove_docker

	config.vm.provision "install-docker", type: "shell", inline: $install_docker

	config.vm.provision "start-docker", type: "shell", run:"always", inline: $start_docker

	## Create postgreSQL docker container
	config.vm.provision "create-postgres", type: "shell", inline: $create_postgres

	## Start postgreSQL
	config.vm.provision "start-postgres", type: "shell", run: "never", inline: $start_postgres

	## Stop postgreSQL
	config.vm.provision "stop-postgres", type: "shell", inline: $stop_postgres

	#Cretate MYSQL docker container
	config.vm.provision "create-mysql", type: "shell", inline: $create_mysql

	## Start MYSQL
	config.vm.provision "start-mysql", type: "shell", run: "never", inline: $start_mysql

	## Stop MYSQL
	config.vm.provision "stop-mysql", type: "shell", inline: $stop_mysql

	## Create MongoDB docker container
	config.vm.provision "create-mongo", type: "shell", inline: $create_mongo

	## Start mongo db
	config.vm.provision "start-mongo", type: "shell", run: "never", "inline": $start_mongo

	## Stop mongo db
	config.vm.provision "stop-mongo", type: "shell", run: "never", "inline": $stop_mongo

	## Create RabbitMQ docker container
	config.vm.provision "create-rabbitmq", type: "shell", inline: $create_rabbitmq

  	## Start RabbitMQ db
  	config.vm.provision "start-rabbitmq", type: "shell", run: "never", "inline": $start_rabbitmq

  	## Stop RabbitMQ db
  	config.vm.provision "stop-rabbitmq", type: "shell", run: "never", "inline": $stop_rabbitmq

	## Create Couchbase docker container
    	config.vm.provision "create-couchbase", type: "shell", inline: $create_couchbase

    	## Start Couchbase
    	config.vm.provision "start-couchbase", type: "shell", run: "never", "inline": $start_couchbase

    	## Stop Couchbase
	config.vm.provision "stop-couchbase", type: "shell", run: "never", "inline": $stop_couchbase
	
	## Create Couchbase docker container
    	config.vm.provision "create-nginx", type: "shell", inline: $create_nginx

    	## Start Couchbase
    	config.vm.provision "start-nginx", type: "shell", run: "never", "inline": $start_nginx

    	## Stop Couchbase
    	config.vm.provision "stop-nginx", type: "shell", run: "never", "inline": $stop_nginx

end
